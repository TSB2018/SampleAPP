<!DOCTYPE html>
<html>
<head>
    <title>A1 - HTML Injection</title>
</head>
<body>

<?$fix=$_GET["fix"];?>

Der hier eingegebene Text wird reflektiert:
<form method="GET" action="">
    <input type="text" name="param"><br>
    <input type="checkbox" name="fix" value="Fix"> Fix<br>
    <input type="submit" name="">
</form>

<div id="divid">
<p>
    <b>Text:</b><i><? 
if($fix)
{ 
    echo htmlentities($_GET["param"]);

}else{
    
    echo $_GET["param"]; 
    
}?></i>
</p>

Beispiele:<br>
<ul>
    <li><?=htmlentities("<script>alert(1);</script>")?></li>
    
    <li><?=htmlentities("<script>console.info(document.cookie);</script>")?></li>
    
</ul>

<p>
<a href="index.php"><- back</a>
</p>
</body>
</html>
